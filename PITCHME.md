
@size[0.6em](Get ready for some)
### Building mobile apps with Flutter
##### A WECODE Code Session
<br>

#### Shuaib Afegbua

---

@quote[For God so love the world, that He gave is only programimg language - Dart, Whoesover codes in it will not perish but will attain code salvations.](Book of Flutter)

---


### INTRODUCTION

* A mobile app development history |
* What is Flutter |
* Why Flutter and Dart |
* Flutter and the future of mobile development |

---

### The Promise




```dart
// I am damn program, you can execute me
main() {
    print("Dart and Flutter will save your soul");
}

```

---
### Dart is an Object Oriented Programming Language

* Encapsulation |
* Abstraction |
* Inheritance |
* Polymorphism |

---
### Variabes and Data Types

---
### Variables
@size[0.6em](Variable refers to a storage location.)

``` dart
var name = 'Abdulhakim';

String country = "Nigeria";

dynamic title = "Senior programmer";

int numOflanguages 

```

@size[0.6em](The identifier on the left stores a reference to the object on the right)

---
#### Naming convention for Identifiers

* Identifiers can start with a letter or underscore (_), followed by any combination of those characters plus digits
* Underscore identifiers are usually used for private members

---

### Data Types

* Numbers |
* strings |
* booleans |
* lists |
* maps |
* runes |
* symbols |

---

### Numbers
@size[0.6em]( Numeric values. int and double/floating point number)

```dart

var age = 23;

int code = 303;

double myBillions = 3,703,000,000.43;

```

---

### strings
@size[0.6em]()

```dart
"I am a damn string"

var name = "Wole"

String title = "Super master"

```

---

### Booleans
@size[0.6em]()

```dart

```

---
### Symbols
@size[0.6em]()

```dart

```


---
# Functions
---

## #Functions
@size[0.6em](Functions are first class citezens.)

@size[0.6em](Functions are object of type Function)

---
#### Named functions

```dart
// Dart function
return_type fuctionName(type param) {
  return param;
}

String sayName(String name) {
  return "Hey $name";
}

// You can avoid the the type anotation
sayName(name) {
  return "Hey $name";
}

// Shorthand => is { return expression}
sayName(name) => return "Hey $name";

```
---

### Named and optional Paremeters

---

#### Named and optional Paremeter

---
#### Main() Function
@size[0.6em](Dart requires every application to have a top level main function which servers as the entry point of the application)

```dart

void main() {
  runApp(...);
}

void main() => runApp(..)

```
---
#### Functions as first-class objects
@size[0.6em](You can pass a function as a parameter to another function)

```dart

void hollaMyName(String name) => print("Your name is $name");

hollaMyName(name) => print("Your name is $name");

void hollaMyName(String name) {
    print("Your name is $name");
}

var seniorProgrammers = ['Wole', 'Kabir', 'Ozo', 'David'];

seniorProgrammers.forEach(hollaMyName);

seniorProgrammers.forEach((name) => print("Your name is $name"));


```
---
#### Functions as first-class objects
@size[0.6em](Assigning a function to a variable)

```dart
var holla = hollaMyName(name) => print("Your name is $name");

assert(holla("Abdulhakim") == "Abdulhakim");

```
---
#### Ananymous functions

```dart

  (){}

  var seniorProgrammers = [‘Wole’, ‘Kabir’, ‘Ozo’, 'David'];

  seniorProgrammers.forEach((name){
      print("$name");
  });


```
---
#### Control flows

Dart supports the following

* if and else
* for loops
* while and do-while loops
* break and continue
* switch and case
* assert

---

## COLLECTION
@size[0.6em]()

* List 
* Set 
* Maps 
* Queue 
* Enum 

---

### List
@size[0.6em](List in Dart are iterable. They are zero based index. It can be a fixed length list and growable list)

```dart
  [1,2,3,4,5,6,]

  var names = ["Habeeb", "Saany", "Ozo", "Omole" ];

  var firstTwenty = List.generate(20, (n) => 1 + n);
  
  var languages = List();
  languages.add("java");
  languages.add("Dart");

  var frameworks = List(3);


```

---

---

### Maps
@size[0.6em]()

```dart
 {
     "key": "value"
 };

 var languages = new Map();
 languages["Java"] = "Old boy";
 language["haskell"] = "I am on steriods";
 language["rust"] = "New kid on the block";
 language["react"] = "Nah, I am not prohgramming language. I am dying";
 print (languages)

 Var citzens = {
     1 : "Buhari",
     2 : "Osinbanjo",
     3 : "Saraki";
 }

Var citzens2 = {
     "Buhari": 1,
     "Osinbanjo": 2,
     "Saraki": 3;
 }
 

```

---

---

### Queues
@size[0.6em]()

```dart

```

---

---

### Enum
@size[0.6em]()

```dart

```
---
## Control Flows and Exception

---
## Exception
* Exception
* Errors
*

```dart


```
---
 ## CLASSES
---
#### Classes

@quote[Every object is an instance of a class, and all classes descend from Object](Dart doc)

A class consist of methods and instance variables/properties


----
#### 

```dart
class Profile {
    String name;
    String language;
    String email;
    int age;

    void displayProfile() => Print("Name: $name Email: $email");
}

Profile habeeb = Profile();

```

---
#### Public and private properties, Getters and Setters

```dart

String get email => _email;
void set email(String value) => _email;

```
---

#### Constructors
* Constructors are used to construct or create instance 
* There are no destructors in Dart; Everything is Garbage collected

```dart

Profile(String name, String email, int age){
    this.name = name;
    this.email = email;
    this.age = age
}
// Dart a simlper approach using the this keyword
Profile(this.name, this.email, this.age)

// Named Constructors
Profile.initialize(){
    name = "Habeeb Kulboi";
    email = "habeeb@wecode.ng";
    age = 80;
}

```
---

#### Inheritance

```dart

class Organiser extends Profile {
    String event;

    void startEvent() => Print("starts the $event");
}
```

---
#### Parent class with constructors

```dart

// Parent class with contructors
class Profile{
  String name;
  String title;

  Profile(this.name, this.title);
}
class Organiser extends Profile{
  String event;

  Organiser(String name, String title, this.event) : super(name, title);
}

// Call,dshfgdsfhdsgfhjsdgfhsdfhsd
?jfsdjdfjnf
  var organiser = Organiser("Hakeem", "Senior Programmer", "Code Sessions");


```
---
#### Abstract class and Interface

* No multiple inheritance

```dart

```
---
 #### package and inputs